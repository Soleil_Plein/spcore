﻿using HarmonyLib;
using System.Reflection;

public class API : IModApi
{
    public static XUiC_ProducerInput CurrentProducerInput;
    public static XUiC_ProducerOutput CurrentProducerOutput;
    public static XUiC_ProducerTools CurrentProducerTools;

    public void InitMod(Mod _modInstance) => Harmony.CreateAndPatchAll(Assembly.GetExecutingAssembly());
}