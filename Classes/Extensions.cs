﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPCore.Classes
{
    public static class Extensions
    {
        public static void Add<T>(this List<T> list, List<T> values)
        {
            foreach (var item in values)
            {
                list.Add(item);
            }
        }
    }
}
