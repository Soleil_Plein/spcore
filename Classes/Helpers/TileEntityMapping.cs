﻿using System.Collections.Generic;

public class TileEntityMapping
{
    public static string Productor = "BlockProductor";
    public static string PoweredProductor = "BlockPoweredProductor";

    public static Dictionary<string, TileEntityType> Types = new Dictionary<string, TileEntityType>()
    {
        // SPCORE
        { Productor, (TileEntityType)70 },
        { PoweredProductor, (TileEntityType)71 },
    };
}