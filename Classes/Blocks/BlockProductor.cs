﻿using SPCore.Classes.SPClasses;
using System;
using System.Collections.Generic;
using UnityEngine;

public class BlockProductor : Block
{
    readonly BlockActivationCommand[] cmds = new BlockActivationCommand[]
    {
        new BlockActivationCommand("open", "campfire", true),
        new BlockActivationCommand("take", "hand", false),
        new BlockActivationCommand("light", "twitch_play", true)
    };

    readonly string PropInputLootList = "InputLootList";
    readonly string PropOutputLootList = "OutputLootList";
    readonly string PropToolsLootList = "ToolsLootList";
    readonly string PropWindowGroup = "WindowGroup";
    string inputLootList;
    string outputLootList;
    string toolsLootList;
    string windowGroup;
    string[] toolTransformNames;

    public BlockProductor() => HasTileEntity = true;

    public List<ProducerRecipe> ProducerRecipes
    {
        get
        {
            if (Properties.Classes.ContainsKey("ProducerRecipes"))
            {
                return ProducerRecipe.GetRecipes(Properties.Classes["ProducerRecipes"]);
            }
            return new List<ProducerRecipe>();
        }
        set => ProducerRecipes = value;
    }

    public override void Init()
    {
        base.Init();
        if (!Properties.Values.ContainsKey(PropInputLootList))
            throw new Exception("Block with name " + GetBlockName() + " doesnt have a input list");
        inputLootList = Properties.Values[PropInputLootList];
        if (!Properties.Values.ContainsKey(PropOutputLootList))
            throw new Exception("Block with name " + GetBlockName() + " doesnt have a output list");
        outputLootList = Properties.Values[PropOutputLootList];
        if (!Properties.Values.ContainsKey(PropToolsLootList))
            throw new Exception("Block with name " + GetBlockName() + " doesnt have a tools list");
        toolsLootList = Properties.Values[PropToolsLootList];
        if (!Properties.Values.ContainsKey(PropWindowGroup))
            throw new Exception("Block with name " + GetBlockName() + " doesnt have a window group");
        windowGroup = Properties.Values[PropWindowGroup];

        string optionalValue = "1,2,3";
        Properties.ParseString("Workstation.ToolNames", ref optionalValue);
        toolTransformNames = optionalValue.Split(new char[] { ',' }, StringSplitOptions.None);
    }

    public override BlockActivationCommand[] GetBlockActivationCommands(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
    {
        bool flag1 = _world.IsMyLandProtectedBlock(_blockPos, _world.GetGameManager().GetPersistentLocalPlayer());
        bool flag3 = ((TileEntityProductor)_world.GetTileEntity(_clrIdx, _blockPos)).IsRunning;
        cmds[1].enabled = flag1;
        cmds[2].icon = flag3 ? "twitch_pause" : "twitch_play";
        return cmds;
    }

    public void TakeItemWithTimer(int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
    {
        if (_blockValue.damage > 0)
        {
            GameManager.ShowTooltip(_player as EntityPlayerLocal, Localization.Get("ttRepairBeforePickup"), string.Empty, "ui_denied");
        }
        else
        {
            LocalPlayerUI playerUi = (_player as EntityPlayerLocal).PlayerUI;
            playerUi.windowManager.Open("timer", true);
            XUiC_Timer childByType = playerUi.xui.GetChildByType<XUiC_Timer>();
            TimerEventData _eventData = new TimerEventData();
            _eventData.Data = new object[4]
            {
                 _cIdx,
                 _blockValue,
                 _blockPos,
                 _player
            };
            _eventData.Event += new TimerEventHandler(EventData_Event);
            childByType.SetTimer(4, _eventData);
        }
    }

    void EventData_Event(TimerEventData timerData)
    {
        World world = GameManager.Instance.World;
        object[] data = (object[])timerData.Data;
        int _clrIdx = (int)data[0];
        BlockValue blockValue = (BlockValue)data[1];
        Vector3i vector3i = (Vector3i)data[2];
        BlockValue block = world.GetBlock(vector3i);
        EntityPlayerLocal entityPlayerLocal = data[3] as EntityPlayerLocal;
        if (block.damage > 0)
            GameManager.ShowTooltip(entityPlayerLocal, Localization.Get("ttRepairBeforePickup"), string.Empty, "ui_denied");
        else if (block.type != blockValue.type)
        {
            GameManager.ShowTooltip(entityPlayerLocal, Localization.Get("ttBlockMissingPickup"), string.Empty, "ui_denied");
        }
        else
        {
            TileEntityProductor tileEntity = world.GetTileEntity(_clrIdx, vector3i) as TileEntityProductor;
            if (tileEntity.IsUserAccessing())
            {
                GameManager.ShowTooltip(entityPlayerLocal, Localization.Get("ttCantPickupInUse"), string.Empty, "ui_denied");
            }
            else
            {
                LocalPlayerUI uiForPlayer = LocalPlayerUI.GetUIForPlayer(entityPlayerLocal);
                HandleTakeInternalItems(tileEntity, uiForPlayer);
                ItemStack itemStack = new ItemStack(block.ToItemValue(), 1);
                if (!uiForPlayer.xui.PlayerInventory.AddItem(itemStack))
                    uiForPlayer.xui.PlayerInventory.DropItem(itemStack);
                world.SetBlockRPC(_clrIdx, vector3i, BlockValue.Air);
            }
        }
    }

    void HandleTakeInternalItems(TileEntityProductor te, LocalPlayerUI playerUI)
    {
        ItemStack[] output = te.OutputItems;
        for (int index = 0; index < output.Length; ++index)
        {
            if (!output[index].IsEmpty() && !playerUI.xui.PlayerInventory.AddItem(output[index]))
                playerUI.xui.PlayerInventory.DropItem(output[index]);
        }
        ItemStack[] tools = te.ToolsItems;
        for (int index = 0; index < tools.Length; ++index)
        {
            if (!tools[index].IsEmpty() && !playerUI.xui.PlayerInventory.AddItem(tools[index]))
                playerUI.xui.PlayerInventory.DropItem(tools[index]);
        }
        ItemStack[] fuel = te.InputItems;
        for (int index = 0; index < fuel.Length; ++index)
        {
            if (!fuel[index].IsEmpty() && !playerUI.xui.PlayerInventory.AddItem(fuel[index]))
                playerUI.xui.PlayerInventory.DropItem(fuel[index]);
        }
    }

    public override void OnBlockAdded(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
    {
        base.OnBlockAdded(world, _chunk, _blockPos, _blockValue);
        if (_blockValue.ischild)
            return;
        TileEntityProductor _te = new TileEntityProductor(_chunk);
        _te.WindowGroup = windowGroup;
        _te.InputList = inputLootList;
        _te.OutputList = outputLootList;
        _te.ToolsList = toolsLootList;
        _te.localChunkPos = World.toBlock(_blockPos);
        _chunk.AddTileEntity(_te);
    }
    
    public override void OnBlockRemoved(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
    {
        base.OnBlockRemoved(world, _chunk, _blockPos, _blockValue);
        if (world.GetTileEntity(_chunk.ClrIdx, _blockPos) is TileEntityProductor tileEntity)
            tileEntity.OnDestroy();
        _chunk.RemoveTileEntityAt<TileEntityProductor>((World)world, World.toBlock(_blockPos));
    }

    public override string GetActivationText(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
    {
        return Localization.Get("useWorkstation");
    }

    public override DestroyedResult OnBlockDestroyedBy(WorldBase _world, int _clrIdx, Vector3i _blockPos, BlockValue _blockValue, int _entityId, bool _bUseHarvestTool)
    {
        TileEntityProductor tileEntityProductor = _world.GetTileEntity(_clrIdx, _blockPos) as TileEntityProductor;
        string windowGroup = tileEntityProductor.WindowGroup;
        tileEntityProductor?.OnDestroy();
        LocalPlayerUI uIForPlayer = LocalPlayerUI.GetUIForPlayer(GameManager.Instance.World.GetEntity(_entityId) as EntityPlayerLocal);
        if (null != uIForPlayer && uIForPlayer.windowManager.IsWindowOpen(windowGroup) && ((XUiC_LootWindow)uIForPlayer.xui.GetWindow(windowGroup).Controller).GetLootBlockPos() == _blockPos)
        {
            uIForPlayer.windowManager.Close(windowGroup);
        }

        if (tileEntityProductor != null)
        {
            //_world.GetGameManager().DropContentOfLootContainerServer(_blockValue, _blockPos, tileEntityProductor.entityId);
        }

        return DestroyedResult.Downgrade;
    }

    public override bool OnBlockActivated(WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
    {
        TileEntityProductor tileEntity = (TileEntityProductor)_world.GetTileEntity(_cIdx, _blockPos);
        if (tileEntity == null)
            return false;
        _player.AimingGun = false;
        Vector3i worldPos = tileEntity.ToWorldPos();
        _world.GetGameManager().TELockServer(_cIdx, worldPos, tileEntity.entityId, _player.entityId);
        return true;
    }

    public override bool OnBlockActivated(string _commandName, WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
    {
        if (_commandName == "open")
            return OnBlockActivated(_world, _cIdx, _blockPos, _blockValue, _player);
        if (_commandName == "take")
        {
            TakeItemWithTimer(_cIdx, _blockPos, _blockValue, _player);
            return true;
        }
        else if (_commandName == "light")
        {
            ActiveProducer(_world, _cIdx, _blockPos);
            return true;
        }
        return false;
    }

    public override bool HasBlockActivationCommands(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
    {
        return true;
    }

    void ActiveProducer(WorldBase _world, int _cIdx, Vector3i _blockPos)
    {
        TileEntityProductor tileEntity = (TileEntityProductor)_world.GetTileEntity(_cIdx, _blockPos);
        if (tileEntity.IsRunning) tileEntity.Stop();
        else if (!tileEntity.IsRunning) tileEntity.Start();
    }

    public void UpdateVisible(TileEntityProductor _tileEntity)
    {
        Transform transform1 = _tileEntity.GetChunk().GetBlockEntity(_tileEntity.ToWorldPos()).transform;
        if (!(bool)transform1)
            return;
        ItemStack[] tools = _tileEntity.ToolsItems;
        int num = Utils.FastMin(tools.Length, toolTransformNames.Length);
        for (int index = 0; index < num; ++index)
        {
            Transform transform2 = transform1.Find(toolTransformNames[index]);
            if ((bool)transform2)
                transform2.gameObject.SetActive(!tools[index].IsEmpty());
        }
        Transform transform3 = transform1.Find("craft");
        if (!(bool)transform3)
            return;
        transform3.gameObject.SetActive(_tileEntity.IsRunning);
    }
}