﻿using HarmonyLib;
using SPCore.Classes;
using SPCore.Classes.SPClasses;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class XUiC_ProducerRecipesList : XUiController
{
    List<XUiC_SPRecipeEntry> recipesEntries;
    public ITileEntityProducer TileEntity;
    XUiV_Grid grid;

    public void SetTileEntity(TileEntityProductor tileEntityProductor) => TileEntity = tileEntityProductor;

    public override void Init()
    {
        base.Init();
        recipesEntries = new List<XUiC_SPRecipeEntry>();
        grid = GetChildById("list").ViewComponent as XUiV_Grid;
        int count = GetChildrenByType<XUiC_SPRecipeEntry>().Length;
        for (int i = 0; i < count; i++)
        {
            recipesEntries.Add(GetChildById(i.ToString()).GetChildByType<XUiC_SPRecipeEntry>());
        }
    }

    public void ResetWindow()
    {
        foreach (var entry in recipesEntries)
        {
            entry.Cleanup();
        }
    }

    string DetailsRecipe(ProducerRecipe producerRecipe)
    {
        StringBuilder sb = new StringBuilder();
        if (producerRecipe.BlockNeeded != null && producerRecipe.BlockNeeded[0] != null)
        {
            sb.Append($"Blocks required in range : {Localization.Get(producerRecipe.BlockNeeded.Join(delimiter: "/"))} in range of {producerRecipe.BlockSearchRange}\n\n");
        }
        sb.Append($"Craft :\n\n");
        for (int i = 0; i < producerRecipe.Items.Length; i++)
        {
            string itemName = producerRecipe.Items[i];
            int itemCount = producerRecipe.ItemsCount[i];
            sb.Append($"({Localization.Get(itemName)}) x{itemCount}\n");
        }
        sb.Append($"\n");
        sb.Append($"Need to have :\n\n");
        if (producerRecipe.ToolRequired[0] != null)
        {
            sb.Append($"Tools :\n");
            foreach (var tool in producerRecipe.ToolRequired)
            {
                sb.Append($"{Localization.Get(tool)}\n");
            }
        }
        sb.Append($"\nIngredients :\n");
        for (int i = 0; i < producerRecipe.Ingredients.Length; i++)
        {
            string itemName = producerRecipe.Ingredients[i];
            int itemCount = producerRecipe.IngredientsCount[i];
            sb.Append($"({Localization.Get(itemName)}) x{itemCount}\n");
        }
        return sb.ToString();
    }

    public void FillGridWithRecipes(ITileEntityProducer tileEntityProductor)
    {
        TileEntity = tileEntityProductor;
        if (TileEntity == null)
        {
            //Log.Warning($"TileEntity is null");
            return;
        }
        var recipes = TileEntity.ProducerRecipes;
        for (int index = 0; index < grid.Rows; index++)
        {
            var entry = recipesEntries[index];
            entry.TileEntity = tileEntityProductor;
            if (index < recipes.Count)
            {
                var recipe = recipes[index];
                if (!recipe.Hidden)
                {
                    //entry.arrow.ToolTip = DetailsRecipe(recipe);
                    if (recipe.IsRandom)
                    {
                        Log.Warning("Recipe is random, hide it");
                        entry.SetVisible(false, Localization.Get(recipe.RandomText));
                    }
                    else
                    {
                        Log.Warning("Recipe is not random, don't hide it");
                        entry.SetVisible(true);
                    }
                    if (!TileEntity.CanCraftRecipe(recipe)) entry.arrow.Color = Color.red;
                    else entry.arrow.Color = Color.green;

                    switch (recipe.Items.Length)
                    {
                        case 1:
                            entry.item1.SpriteName = ItemClass.GetItem(recipe.Items[0]).ItemClass.GetIconName();
                            break;
                        case 2:
                            entry.item1.SpriteName = ItemClass.GetItem(recipe.Items[0]).ItemClass.GetIconName();
                            entry.item2.SpriteName = ItemClass.GetItem(recipe.Items[1]).ItemClass.GetIconName();
                            break;
                        case 3:
                            entry.item1.SpriteName = ItemClass.GetItem(recipe.Items[0]).ItemClass.GetIconName();
                            entry.item2.SpriteName = ItemClass.GetItem(recipe.Items[1]).ItemClass.GetIconName();
                            entry.item3.SpriteName = ItemClass.GetItem(recipe.Items[2]).ItemClass.GetIconName();
                            break;
                        case 4:
                            entry.item1.SpriteName = ItemClass.GetItem(recipe.Items[0]).ItemClass.GetIconName();
                            entry.item2.SpriteName = ItemClass.GetItem(recipe.Items[1]).ItemClass.GetIconName();
                            entry.item3.SpriteName = ItemClass.GetItem(recipe.Items[2]).ItemClass.GetIconName();
                            entry.item4.SpriteName = ItemClass.GetItem(recipe.Items[3]).ItemClass.GetIconName();
                            break;
                        case 5:
                            entry.item1.SpriteName = ItemClass.GetItem(recipe.Items[0]).ItemClass.GetIconName();
                            entry.item2.SpriteName = ItemClass.GetItem(recipe.Items[1]).ItemClass.GetIconName();
                            entry.item3.SpriteName = ItemClass.GetItem(recipe.Items[2]).ItemClass.GetIconName();
                            entry.item4.SpriteName = ItemClass.GetItem(recipe.Items[3]).ItemClass.GetIconName();
                            entry.item5.SpriteName = ItemClass.GetItem(recipe.Items[4]).ItemClass.GetIconName();
                            break;
                        case 6:
                            entry.item1.SpriteName = ItemClass.GetItem(recipe.Items[0]).ItemClass.GetIconName();
                            entry.item2.SpriteName = ItemClass.GetItem(recipe.Items[1]).ItemClass.GetIconName();
                            entry.item3.SpriteName = ItemClass.GetItem(recipe.Items[2]).ItemClass.GetIconName();
                            entry.item4.SpriteName = ItemClass.GetItem(recipe.Items[3]).ItemClass.GetIconName();
                            entry.item5.SpriteName = ItemClass.GetItem(recipe.Items[4]).ItemClass.GetIconName();
                            entry.item6.SpriteName = ItemClass.GetItem(recipe.Items[5]).ItemClass.GetIconName();
                            break;
                        case 7:
                            entry.item1.SpriteName = ItemClass.GetItem(recipe.Items[0]).ItemClass.GetIconName();
                            entry.item2.SpriteName = ItemClass.GetItem(recipe.Items[1]).ItemClass.GetIconName();
                            entry.item3.SpriteName = ItemClass.GetItem(recipe.Items[2]).ItemClass.GetIconName();
                            entry.item4.SpriteName = ItemClass.GetItem(recipe.Items[3]).ItemClass.GetIconName();
                            entry.item5.SpriteName = ItemClass.GetItem(recipe.Items[4]).ItemClass.GetIconName();
                            entry.item6.SpriteName = ItemClass.GetItem(recipe.Items[5]).ItemClass.GetIconName();
                            entry.item7.SpriteName = ItemClass.GetItem(recipe.Items[6]).ItemClass.GetIconName();
                            break;
                        case 8:
                            entry.item1.SpriteName = ItemClass.GetItem(recipe.Items[0]).ItemClass.GetIconName();
                            entry.item2.SpriteName = ItemClass.GetItem(recipe.Items[1]).ItemClass.GetIconName();
                            entry.item3.SpriteName = ItemClass.GetItem(recipe.Items[2]).ItemClass.GetIconName();
                            entry.item4.SpriteName = ItemClass.GetItem(recipe.Items[3]).ItemClass.GetIconName();
                            entry.item5.SpriteName = ItemClass.GetItem(recipe.Items[4]).ItemClass.GetIconName();
                            entry.item6.SpriteName = ItemClass.GetItem(recipe.Items[5]).ItemClass.GetIconName();
                            entry.item7.SpriteName = ItemClass.GetItem(recipe.Items[6]).ItemClass.GetIconName();
                            entry.item8.SpriteName = ItemClass.GetItem(recipe.Items[7]).ItemClass.GetIconName();
                            break;
                        case 9:
                            entry.item1.SpriteName = ItemClass.GetItem(recipe.Items[0]).ItemClass.GetIconName();
                            entry.item2.SpriteName = ItemClass.GetItem(recipe.Items[1]).ItemClass.GetIconName();
                            entry.item3.SpriteName = ItemClass.GetItem(recipe.Items[2]).ItemClass.GetIconName();
                            entry.item4.SpriteName = ItemClass.GetItem(recipe.Items[3]).ItemClass.GetIconName();
                            entry.item5.SpriteName = ItemClass.GetItem(recipe.Items[4]).ItemClass.GetIconName();
                            entry.item6.SpriteName = ItemClass.GetItem(recipe.Items[5]).ItemClass.GetIconName();
                            entry.item7.SpriteName = ItemClass.GetItem(recipe.Items[6]).ItemClass.GetIconName();
                            entry.item8.SpriteName = ItemClass.GetItem(recipe.Items[7]).ItemClass.GetIconName();
                            entry.item9.SpriteName = ItemClass.GetItem(recipe.Items[8]).ItemClass.GetIconName();
                            break;
                        case 10:
                            entry.item1.SpriteName = ItemClass.GetItem(recipe.Items[0]).ItemClass.GetIconName();
                            entry.item2.SpriteName = ItemClass.GetItem(recipe.Items[1]).ItemClass.GetIconName();
                            entry.item3.SpriteName = ItemClass.GetItem(recipe.Items[2]).ItemClass.GetIconName();
                            entry.item4.SpriteName = ItemClass.GetItem(recipe.Items[3]).ItemClass.GetIconName();
                            entry.item5.SpriteName = ItemClass.GetItem(recipe.Items[4]).ItemClass.GetIconName();
                            entry.item6.SpriteName = ItemClass.GetItem(recipe.Items[5]).ItemClass.GetIconName();
                            entry.item7.SpriteName = ItemClass.GetItem(recipe.Items[6]).ItemClass.GetIconName();
                            entry.item8.SpriteName = ItemClass.GetItem(recipe.Items[7]).ItemClass.GetIconName();
                            entry.item9.SpriteName = ItemClass.GetItem(recipe.Items[8]).ItemClass.GetIconName();
                            entry.item10.SpriteName = ItemClass.GetItem(recipe.Items[9]).ItemClass.GetIconName();
                            break;
                    }

                    switch (recipe.Items.Length)
                    {
                        case 1:
                            entry.itemCount1.Text = recipe.ItemsCount[0].ToString();
                            break;
                        case 2:
                            entry.itemCount1.Text = recipe.ItemsCount[0].ToString();
                            entry.itemCount2.Text = recipe.ItemsCount[1].ToString();
                            break;
                        case 3:
                            entry.itemCount1.Text = recipe.ItemsCount[0].ToString();
                            entry.itemCount2.Text = recipe.ItemsCount[1].ToString();
                            entry.itemCount3.Text = recipe.ItemsCount[2].ToString();
                            break;
                        case 4:
                            entry.itemCount1.Text = recipe.ItemsCount[0].ToString();
                            entry.itemCount2.Text = recipe.ItemsCount[1].ToString();
                            entry.itemCount3.Text = recipe.ItemsCount[2].ToString();
                            entry.itemCount4.Text = recipe.ItemsCount[3].ToString();
                            break;
                        case 5:
                            entry.itemCount1.Text = recipe.ItemsCount[0].ToString();
                            entry.itemCount2.Text = recipe.ItemsCount[1].ToString();
                            entry.itemCount3.Text = recipe.ItemsCount[2].ToString();
                            entry.itemCount4.Text = recipe.ItemsCount[3].ToString();
                            entry.itemCount5.Text = recipe.ItemsCount[4].ToString();
                            break;
                        case 6:
                            entry.itemCount1.Text = recipe.ItemsCount[0].ToString();
                            entry.itemCount2.Text = recipe.ItemsCount[1].ToString();
                            entry.itemCount3.Text = recipe.ItemsCount[2].ToString();
                            entry.itemCount4.Text = recipe.ItemsCount[3].ToString();
                            entry.itemCount5.Text = recipe.ItemsCount[4].ToString();
                            entry.itemCount6.Text = recipe.ItemsCount[5].ToString();
                            break;
                        case 7:
                            entry.itemCount1.Text = recipe.ItemsCount[0].ToString();
                            entry.itemCount2.Text = recipe.ItemsCount[1].ToString();
                            entry.itemCount3.Text = recipe.ItemsCount[2].ToString();
                            entry.itemCount4.Text = recipe.ItemsCount[3].ToString();
                            entry.itemCount5.Text = recipe.ItemsCount[4].ToString();
                            entry.itemCount6.Text = recipe.ItemsCount[5].ToString();
                            entry.itemCount7.Text = recipe.ItemsCount[6].ToString();
                            break;
                        case 8:
                            entry.itemCount1.Text = recipe.ItemsCount[0].ToString();
                            entry.itemCount2.Text = recipe.ItemsCount[1].ToString();
                            entry.itemCount3.Text = recipe.ItemsCount[2].ToString();
                            entry.itemCount4.Text = recipe.ItemsCount[3].ToString();
                            entry.itemCount5.Text = recipe.ItemsCount[4].ToString();
                            entry.itemCount6.Text = recipe.ItemsCount[5].ToString();
                            entry.itemCount7.Text = recipe.ItemsCount[6].ToString();
                            entry.itemCount8.Text = recipe.ItemsCount[7].ToString();
                            break;
                        case 9:
                            entry.itemCount1.Text = recipe.ItemsCount[0].ToString();
                            entry.itemCount2.Text = recipe.ItemsCount[1].ToString();
                            entry.itemCount3.Text = recipe.ItemsCount[2].ToString();
                            entry.itemCount4.Text = recipe.ItemsCount[3].ToString();
                            entry.itemCount5.Text = recipe.ItemsCount[4].ToString();
                            entry.itemCount6.Text = recipe.ItemsCount[5].ToString();
                            entry.itemCount7.Text = recipe.ItemsCount[6].ToString();
                            entry.itemCount8.Text = recipe.ItemsCount[7].ToString();
                            entry.itemCount9.Text = recipe.ItemsCount[8].ToString();
                            break;
                        case 10:
                            entry.itemCount1.Text = recipe.ItemsCount[0].ToString();
                            entry.itemCount2.Text = recipe.ItemsCount[1].ToString();
                            entry.itemCount3.Text = recipe.ItemsCount[2].ToString();
                            entry.itemCount4.Text = recipe.ItemsCount[3].ToString();
                            entry.itemCount5.Text = recipe.ItemsCount[4].ToString();
                            entry.itemCount6.Text = recipe.ItemsCount[5].ToString();
                            entry.itemCount7.Text = recipe.ItemsCount[6].ToString();
                            entry.itemCount8.Text = recipe.ItemsCount[7].ToString();
                            entry.itemCount9.Text = recipe.ItemsCount[8].ToString();
                            entry.itemCount10.Text = recipe.ItemsCount[9].ToString();
                            break;
                    }
                }
                else entry.ViewComponent.IsVisible = false;
            }
            else
            {
                entry.ViewComponent.IsVisible = false;
            }
        }
    }
}