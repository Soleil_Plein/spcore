﻿using Audio;
using UnityEngine;

public class XUiC_ProducerWindowGroup : XUiController
{
    XUiC_WindowNonPagingHeader headerWindow;
    XUiC_ProducerRecipesList recipesListWindow;
    XUiC_ProducerInput inputWindow;
    XUiC_ProducerOutput outputWindow;
    XUiC_ProducerTools toolsWindow;
    XUiC_RecipeInfoWindow recipeInfoWindow;
    ///XUiC_ProducerController producerController;
    bool activeKeyDown;
    bool wasReleased;
    public ITileEntityProducer TileEntity;

    public void SetTileEntity(ITileEntityProducer tileEntityProductor) => TileEntity = tileEntityProductor;

    public override void Init()
    {
        base.Init();
        //producerController = GetChildByType<XUiC_ProducerController>();
        recipeInfoWindow = xui.GetChildByType<XUiC_RecipeInfoWindow>();
        headerWindow = GetChildByType<XUiC_WindowNonPagingHeader>();
        recipesListWindow = GetChildByType<XUiC_ProducerRecipesList>();
        inputWindow = GetChildByType<XUiC_ProducerInput>();
        outputWindow = GetChildByType<XUiC_ProducerOutput>();
        toolsWindow = GetChildByType<XUiC_ProducerTools>();
    }

    public override void OnOpen()
    {
        base.OnOpen();
        bool hasToolsSlots;
        recipesListWindow.FillGridWithRecipes(TileEntity);
        inputWindow.SetInputSlots(TileEntity, TileEntity.InputItems);
        outputWindow.SetOutputSlots(TileEntity, TileEntity.OutputItems);
        API.CurrentProducerTools = toolsWindow;
        API.CurrentProducerInput = inputWindow;
        API.CurrentProducerOutput = outputWindow;
        recipeInfoWindow.SetTileEntity(TileEntity);
        //producerController.SetTileEntity(TileEntity);
        if (TileEntity.AllowedToolsItems != null) toolsWindow.SetToolsSlots(TileEntity, TileEntity.ToolsItems);
        var te = TileEntity as TileEntity;
        if (!te.blockValue.Block.Properties.Values.ContainsKey("AllowedToolsItems")) hasToolsSlots = false;
        else hasToolsSlots = te.blockValue.Block.Properties.Values["AllowedToolsItems"] != string.Empty;
        
        if (headerWindow != null)
        {
            string name = "";
            EntityPlayer entityPlayer = xui.playerUI.entityPlayer;
            name = Localization.Get(te.blockValue.Block.GetBlockName());
            headerWindow.SetHeader(name);
        }
        if (ViewComponent != null && !ViewComponent.IsVisible)
        {
            ViewComponent.OnOpen();
            ViewComponent.IsVisible = true;
        }
        if (!hasToolsSlots)
        {
            API.CurrentProducerTools = null;
            toolsWindow.ViewComponent.IsVisible = false;
        }
        xui.RecenterWindowGroup(windowGroup);
        Manager.BroadcastPlayByLocalPlayer(TileEntity.ToWorldPos().ToVector3() + Vector3.one * 0.5f, "open_vending");
        TileEntity.SetWindows(inputWindow, outputWindow, toolsWindow, recipesListWindow, recipeInfoWindow);
        te.Destroyed += new XUiEvent_TileEntityDestroyed(TileEntity_Destroyed);
        IsDirty = true;
        TileEntity.SetModified();
    }

    public override void Update(float _dt)
    {
        base.Update(_dt);
        if (!windowGroup.isShowing)
            return;
        if (!xui.playerUI.playerInput.PermanentActions.Activate.IsPressed)
            wasReleased = true;
        if (!wasReleased)
            return;
        if (xui.playerUI.playerInput.PermanentActions.Activate.IsPressed)
            activeKeyDown = true;
        if (!xui.playerUI.playerInput.PermanentActions.Activate.WasReleased || !activeKeyDown)
            return;
        activeKeyDown = false;
        if (xui.playerUI.windowManager.IsInputActive())
            return;
        xui.playerUI.windowManager.CloseAllOpenWindows();
    }

    public override void OnClose()
    {
        base.OnClose();
        wasReleased = false;
        activeKeyDown = false;
        Manager.BroadcastPlayByLocalPlayer(TileEntity.ToWorldPos().ToVector3() + Vector3.one * 0.5f, "close_vending");
        (TileEntity as TileEntity).Destroyed -= new XUiEvent_TileEntityDestroyed(TileEntity_Destroyed);
        TileEntity.SetModified();
        TileEntity.SetWindows(null, null, null, null, null);
        inputWindow.ResetInputWindow();
        outputWindow.ResetOutputWindow();
        toolsWindow.ResetToolsWindow();
        recipesListWindow.ResetWindow();
        API.CurrentProducerTools = null;
        API.CurrentProducerInput = null;
        API.CurrentProducerOutput = null;
        GameManager.Instance.TEUnlockServer((TileEntity as TileEntity).GetClrIdx(), TileEntity.ToWorldPos(), TileEntity.EntityId);
    }

    void TileEntity_Destroyed(TileEntity te)
    {
        if (TileEntity == te)
        {
            if (GameManager.Instance == null)
                return;
            xui.playerUI.windowManager.Close((te as ITileEntityProducer).WindowGroup);
        }
        else
            te.Destroyed -= new XUiEvent_TileEntityDestroyed(TileEntity_Destroyed);
    }
}