﻿using SPCore.Classes;
using UnityEngine;

public class XUiC_RecipeInfoToolEntry : XUiController
{
    public XUiV_Sprite ToolSprite;
    public XUiV_Sprite ToolState;
    public ITileEntityProducer TileEntity;

    public override void Init()
    {
        base.Init();
        ToolSprite = GetChildById("Tool").ViewComponent as XUiV_Sprite;
        ToolState = GetChildById("ToolState").ViewComponent as XUiV_Sprite;
    }

    public void SetSprite(string toolIcon, string itemLocalizedName, Color color)
    {
        ToolSprite.SpriteName = toolIcon;
        ToolSprite.ToolTip = itemLocalizedName;
        ToolSprite.Color = color;
    }

    public void SetToolState(string toolName)
    {
        if (TileEntity.ContainerHaveTool(toolName))
        {
            ToolState.SpriteName = "ui_game_symbol_check";
            ToolState.Color = Color.green;
        }
        else
        {
            ToolState.SpriteName = "ui_game_symbol_x";
            ToolState.Color = Color.red;
        }
    }

    public override void Cleanup()
    {
        base.Cleanup();
        ToolSprite.SpriteName = string.Empty;
        ToolSprite.ToolTip = string.Empty;
        ToolState.SpriteName = string.Empty;
    }
}