﻿using UnityEngine;

public class XUiC_RecipeInfoBlockEntry : XUiController
{
    public XUiV_Sprite BlockSprite;
    public XUiV_Label BlockCount;
    public ITileEntityProducer TileEntity;

    public override void Init()
    {
        base.Init();
        BlockSprite = GetChildById("Block").ViewComponent as XUiV_Sprite;
        BlockCount = GetChildById("BlockCount").ViewComponent as XUiV_Label;
    }

    public override void Cleanup()
    {
        base.Cleanup();
        BlockSprite.SpriteName = string.Empty;
        BlockCount.Text = string.Empty;
        BlockSprite.ToolTip = string.Empty;
    }

    public void SetBlock(string blockIcon, string blockName, string itemLocalizedName, int blockCount, int searchRange, Color color)
    {
        BlockSprite.SpriteName = blockIcon;
        BlockSprite.ToolTip = itemLocalizedName;
        BlockSprite.Color = color;
        if (TileEntity.CheckNearbyBlock(blockName, blockCount, searchRange, out int result))
        {
            BlockCount.Color = Color.green;
            BlockCount.Text = $"{blockCount}";
        }
        else
        {
            BlockCount.Color = Color.red;
            BlockCount.Text = $"{result}/{blockCount}";
        }
    }
}