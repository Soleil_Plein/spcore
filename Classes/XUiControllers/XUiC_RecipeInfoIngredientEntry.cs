﻿using UnityEngine;

public class XUiC_RecipeInfoIngredientEntry : XUiController
{
    public XUiV_Sprite IngredientSprite;
    public XUiV_Label IngredientCount;
    public ITileEntityProducer TileEntity;

    public override void Init()
    {
        base.Init();
        IngredientCount = GetChildById("IngredientCount").ViewComponent as XUiV_Label;
        IngredientSprite = GetChildById("Ingredient").ViewComponent as XUiV_Sprite;
    }

    public void SetSprite(string ingredientIcon, string itemLocalizedName, Color color)
    {
        IngredientSprite.SpriteName = ingredientIcon;
        IngredientSprite.ToolTip = itemLocalizedName;
        IngredientSprite.Color = color;
    }

    public void SetIngredientCount(string itemName, int ingredientCount)
    {
        IngredientCount.Text = ingredientCount.ToString();
        if (TileEntity.ContainerHasItem(ItemClass.GetItem(itemName), ingredientCount))
        {
            IngredientCount.Color = Color.green;
        }
        else IngredientCount.Color = Color.red;
    }

    public override void Cleanup()
    {
        base.Cleanup();
        IngredientCount.Text = string.Empty;
        IngredientSprite.SpriteName = string.Empty;
        IngredientSprite.ToolTip = string.Empty;
    }
}