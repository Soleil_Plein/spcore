﻿using SPCore.Classes;
using UnityEngine;

public class XUiC_RecipeInfoItemEntry : XUiController
{
    public XUiV_Sprite ItemSprite;
    public XUiV_Label ItemCount;
    public ITileEntityProducer TileEntity;

    public override void Init()
    {
        base.Init();
        ItemSprite = GetChildById("Item").ViewComponent as XUiV_Sprite;
        ItemCount = GetChildById("ItemCount").ViewComponent as XUiV_Label;
    }

    public void SetItem(string itemIcon, string itemLocalizedName, int count, Color color)
    {
        ItemSprite.SpriteName = itemIcon;
        ItemSprite.Color = color;
        ItemCount.Text = count.ToString();
        ItemSprite.ToolTip = itemLocalizedName;
    }

    public override void Cleanup()
    {
        base.Cleanup();
        ItemSprite.SpriteName = string.Empty;
        ItemCount.Text = string.Empty;
        ItemSprite.ToolTip = string.Empty;
    }
}