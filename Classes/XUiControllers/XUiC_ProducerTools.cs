﻿public class XUiC_ProducerTools : XUiC_ProducerLootContainer
{
    public bool TryAddTool(ItemClass newItemClass, ItemStack newItemStack)
    {
        for (int i = 0; i < itemControllers.Length; i++)
        {
            XUiC_RequiredItemStack xUiC_RequiredItemStack = (XUiC_RequiredItemStack)itemControllers[i];
            if (xUiC_RequiredItemStack.RequiredItemClass == newItemClass && xUiC_RequiredItemStack.ItemStack.IsEmpty())
            {
                xUiC_RequiredItemStack.ItemStack = newItemStack.Clone();
                TileEntity.SetModified();
                return true;
            }
        }
        return false;
    }
}