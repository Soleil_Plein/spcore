﻿using SPCore.Classes.SPClasses;

public class XUiC_SPRecipeEntry : XUiC_SelectableEntry
{
    public XUiV_Sprite arrow;
    public XUiV_Sprite item1;
    public XUiV_Sprite item2;
    public XUiV_Sprite item3;
    public XUiV_Sprite item4;
    public XUiV_Sprite item5;
    public XUiV_Sprite item6;
    public XUiV_Sprite item7;
    public XUiV_Sprite item8;
    public XUiV_Sprite item9;
    public XUiV_Sprite item10;
    public XUiV_Label itemCount1;
    public XUiV_Label itemCount2;
    public XUiV_Label itemCount3;
    public XUiV_Label itemCount4;
    public XUiV_Label itemCount5;
    public XUiV_Label itemCount6;
    public XUiV_Label itemCount7;
    public XUiV_Label itemCount8;
    public XUiV_Label itemCount9;
    public XUiV_Label itemCount10;
    public XUiV_Label randomLabel;
    
    public XUiC_RecipeInfoWindow RecipeInfoWindow;
    public ITileEntityProducer TileEntity;

    public override void Init()
    {
        base.Init();
        XUiController childById0 = GetChildById("Arrow");
        arrow = childById0.ViewComponent as XUiV_Sprite;
        XUiController childById11 = GetChildById("Item1");
        item1 = childById11.ViewComponent as XUiV_Sprite;
        XUiController childById12 = GetChildById("Item2");
        item2 = childById12.ViewComponent as XUiV_Sprite;
        XUiController childById13 = GetChildById("Item3");
        item3 = childById13.ViewComponent as XUiV_Sprite;
        XUiController childById14 = GetChildById("Item4");
        item4 = childById14.ViewComponent as XUiV_Sprite;
        XUiController childById15 = GetChildById("Item5");
        item5 = childById15.ViewComponent as XUiV_Sprite;
        XUiController childById16 = GetChildById("Item6");
        item6 = childById16.ViewComponent as XUiV_Sprite;
        XUiController childById17 = GetChildById("Item7");
        item7 = childById17.ViewComponent as XUiV_Sprite;
        XUiController childById18 = GetChildById("Item8");
        item8 = childById18.ViewComponent as XUiV_Sprite;
        XUiController childById19 = GetChildById("Item9");
        item9 = childById19.ViewComponent as XUiV_Sprite;
        XUiController childById20 = GetChildById("Item10");
        item10 = childById20.ViewComponent as XUiV_Sprite;
        XUiController childById21 = GetChildById("ItemCount1");
        itemCount1 = childById21.ViewComponent as XUiV_Label;
        XUiController childById22 = GetChildById("ItemCount2");
        itemCount2 = childById22.ViewComponent as XUiV_Label;
        XUiController childById23 = GetChildById("ItemCount3");
        itemCount3 = childById23.ViewComponent as XUiV_Label;
        XUiController childById24 = GetChildById("ItemCount4");
        itemCount4 = childById24.ViewComponent as XUiV_Label;
        XUiController childById25 = GetChildById("ItemCount5");
        itemCount5 = childById25.ViewComponent as XUiV_Label;
        XUiController childById26 = GetChildById("ItemCount6");
        itemCount6 = childById26.ViewComponent as XUiV_Label;
        XUiController childById27 = GetChildById("ItemCount7");
        itemCount7 = childById27.ViewComponent as XUiV_Label;
        XUiController childById28 = GetChildById("ItemCount8");
        itemCount8 = childById28.ViewComponent as XUiV_Label;
        XUiController childById29 = GetChildById("ItemCount9");
        itemCount9 = childById29.ViewComponent as XUiV_Label;
        XUiController childById30 = GetChildById("ItemCount10");
        itemCount10 = childById30.ViewComponent as XUiV_Label;
        XUiController childById31 = GetChildById("RandomLabel");
        randomLabel = childById31.ViewComponent as XUiV_Label;
    }

    public override void Cleanup()
    {
        base.Cleanup();
        item1.SpriteName = string.Empty;
        item2.SpriteName = string.Empty;
        item3.SpriteName = string.Empty;
        item4.SpriteName = string.Empty;
        item5.SpriteName = string.Empty;
        item6.SpriteName = string.Empty;
        item7.SpriteName = string.Empty;
        item8.SpriteName = string.Empty;
        item9.SpriteName = string.Empty;
        item10.SpriteName = string.Empty;

        item1.ToolTip = string.Empty;
        item2.ToolTip = string.Empty;
        item3.ToolTip = string.Empty;
        item4.ToolTip = string.Empty;
        item5.ToolTip = string.Empty;
        item6.ToolTip = string.Empty;
        item7.ToolTip = string.Empty;
        item8.ToolTip = string.Empty;
        item9.ToolTip = string.Empty;
        item10.ToolTip = string.Empty;

        itemCount1.Text = string.Empty;
        itemCount2.Text = string.Empty;
        itemCount3.Text = string.Empty;
        itemCount4.Text = string.Empty;
        itemCount5.Text = string.Empty;
        itemCount6.Text = string.Empty;
        itemCount7.Text = string.Empty;
        itemCount8.Text = string.Empty;
        itemCount9.Text = string.Empty;
        itemCount10.Text = string.Empty;

        arrow.ToolTip = string.Empty;
        randomLabel.Text = string.Empty;
    }

    public override void OnOpen()
    {
        base.OnOpen();
        RecipeInfoWindow = xui.GetChildByType<XUiC_RecipeInfoWindow>();
    }

    public void SetVisible(bool _flag, string _text = null)
    {
        GetChildById("ItemsGrid").ViewComponent.IsVisible = _flag;
        randomLabel.IsVisible = !_flag;
        if (_text != null)
        {
            randomLabel.Text = _text;
        }
    }

    protected override void OnPressed(int _mouseButton)
    {
        base.OnPressed(_mouseButton);
        RecipeInfoWindow.ViewComponent.IsVisible = true;
        var recipe = SelectRecipe(StringParsers.ParseSInt32(ViewComponent.ID));
        //Log.Warning(recipe.ToString());
        RecipeInfoWindow.Cleanup();
        RecipeInfoWindow.SetWindow(recipe);
    }

    ProducerRecipe SelectRecipe(int id)
    {
        int index = 0;
        foreach (var recipe in TileEntity.ProducerRecipes)
        {
            if (id == index)
            {
                return recipe;
            }
            index++;
        }
        return null;
    }
}