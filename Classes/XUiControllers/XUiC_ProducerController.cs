﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class XUiC_ProducerController : XUiController
{
    XUiV_Button PauseButton;
    ITileEntityProducer TileEntity;

    public override void Init()
    {
        base.Init();
        PauseButton = GetChildById("PauseButton").ViewComponent as XUiV_Button;
        PauseButton.Controller.OnPress += Controller_OnPress;
    }

    void Controller_OnPress(XUiController _sender, int _mouseButton)
    {
        if (TileEntity.IsRunning)
        {
            PauseButton.CurrentSpriteName = "ui_game_symbol_twitch_play";
            PauseButton.HoverSpriteName = "ui_game_symbol_twitch_play";
            PauseButton.DefaultSpriteName = "ui_game_symbol_twitch_play";
            //Log.Warning("Stop producer");
            TileEntity.Stop();
        }
        else if (!TileEntity.IsRunning)
        {
            PauseButton.CurrentSpriteName = "ui_game_symbol_twitch_pause";
            PauseButton.HoverSpriteName = "ui_game_symbol_twitch_pause";
            PauseButton.DefaultSpriteName = "ui_game_symbol_twitch_pause";
            //Log.Warning("Start producer");
            TileEntity.Start();
        }
    }

    public override bool GetBindingValue(ref string _value, string _bindingName)
    {
        switch (_bindingName)
        {
            case "pauseicon":
                _value = TileEntity != null && TileEntity.IsRunning ? "ui_game_symbol_twitch_pause" : "ui_game_symbol_twitch_play";
                return true;
        }
        return false;
    }

    /*public override void OnOpen()
    {
        if (TileEntity != null && TileEntity.IsRunning)
        {
            PauseButton.CurrentSpriteName = "ui_game_symbol_twitch_pause";
            PauseButton.HoverSpriteName = "ui_game_symbol_twitch_pause";
            PauseButton.DefaultSpriteName = "ui_game_symbol_twitch_pause";
        }
        else if (TileEntity != null && !TileEntity.IsRunning)
        {
            PauseButton.CurrentSpriteName = "ui_game_symbol_twitch_play";
            PauseButton.HoverSpriteName = "ui_game_symbol_twitch_play";
            PauseButton.DefaultSpriteName = "ui_game_symbol_twitch_play";
        }
    }*/

    public void SetTileEntity(ITileEntityProducer _tileEntity) => TileEntity = _tileEntity;
}