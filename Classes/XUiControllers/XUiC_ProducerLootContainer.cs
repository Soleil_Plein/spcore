﻿using SPCore.Classes;
using System.Reflection;

public class XUiC_ProducerLootContainer : XUiC_ItemStackGrid
{
    public ITileEntityProducer TileEntity;

    public Vector2i GridCellSize { get; set; }

    protected override void UpdateBackend(ItemStack[] stackList)
    {
    }

    protected override void SetStacks(ItemStack[] stackList)
    {
    }

    public void SetToolsSlots(ITileEntityProducer lootContainer, ItemStack[] stackList)
    {
        if (stackList == null)
            return;
        TileEntity = lootContainer;
        if (TileEntity == null || TileEntity.ToolsItems == null || TileEntity.AllowedToolsItems[0] == null || TileEntity.AllowedToolsItems == null) return;
        items = TileEntity.ToolsItems;
        string[] strArray = TileEntity.AllowedToolsItems;
        XUiC_ItemInfoWindow childByType = xui.GetChildByType<XUiC_ItemInfoWindow>();
        XUiV_Grid viewComponent = (XUiV_Grid)this.viewComponent;
        int length = stackList.Length;
        viewComponent.Columns = LootContainer.GetLootContainer(lootContainer.ToolsList).size.x;
        viewComponent.Rows = LootContainer.GetLootContainer(lootContainer.ToolsList).size.y;
        for (int index = 0; index < itemControllers.Length; ++index)
        {
            XUiC_RequiredItemStack itemController = (XUiC_RequiredItemStack)itemControllers[index];
            if (index < length) itemController.RequiredItemClass = ItemClass.GetItem(strArray[index]).ItemClass;
            itemController.RequiredItemOnly = true;
            itemController.InfoWindow = childByType;
            itemController.SlotNumber = index;
            itemController.SlotChangedEvent -= new XUiEvent_SlotChangedEventHandler(HandleToolSlotChangedEvent);
            itemController.StackLocation = XUiC_ItemStack.StackLocationTypes.LootContainer;
            if (index < length)
            {
                itemController.ForceSetItemStack(TileEntity.ToolsItems[index].Clone());
                itemControllers[index].ViewComponent.IsVisible = true;
                itemController.SlotChangedEvent += new XUiEvent_SlotChangedEventHandler(HandleToolSlotChangedEvent);
            }
            else
            {
                itemController.ItemStack = ItemStack.Empty.Clone();
                itemControllers[index].ViewComponent.IsVisible = false;
            }
        }
        windowGroup.Controller.SetAllChildrenDirty();
    }

    public void ResetInputWindow()
    {
        for (int i = 0; i < itemControllers.Length; i++)
        {
            XUiC_ItemStack itemController = (XUiC_ItemStack)itemControllers[i];
            itemController.SlotChangedEvent -= new XUiEvent_SlotChangedEventHandler(HandleInputSlotChangedEvent);
            itemController.ItemStack = ItemStack.Empty.Clone();
            itemControllers[i].ViewComponent.IsVisible = false;
        }
    }

    public void ResetOutputWindow()
    {
        for (int i = 0; i < itemControllers.Length; i++)
        {
            XUiC_ItemStack itemController = (XUiC_ItemStack)itemControllers[i];
            itemController.SlotChangedEvent -= new XUiEvent_SlotChangedEventHandler(HandleOutputSlotChangedEvent);
            itemController.ItemStack = ItemStack.Empty.Clone();
            itemControllers[i].ViewComponent.IsVisible = false;
        }
    }

    public void ResetToolsWindow()
    {
        for (int i = 0; i < itemControllers.Length; i++)
        {
            XUiC_RequiredItemStack itemController = (XUiC_RequiredItemStack)itemControllers[i];
            itemController.SlotChangedEvent -= new XUiEvent_SlotChangedEventHandler(HandleToolSlotChangedEvent);
            itemController.ItemStack = ItemStack.Empty.Clone();
            itemController.RequiredItemClass = null;
            itemControllers[i].ViewComponent.IsVisible = false;
        }
    }

    public void SetInputSlots(ITileEntityProducer lootContainer, ItemStack[] stackList)
    {
        if (stackList == null)
            return;
        TileEntity = lootContainer;
        if (TileEntity == null || TileEntity.InputItems == null) return;
        items = TileEntity.InputItems;
        XUiC_ItemInfoWindow childByType = xui.GetChildByType<XUiC_ItemInfoWindow>();
        XUiV_Grid viewComponent = (XUiV_Grid)this.viewComponent;
        viewComponent.Columns = LootContainer.GetLootContainer(lootContainer.InputList).size.x;
        viewComponent.Rows = LootContainer.GetLootContainer(lootContainer.InputList).size.y;
        int length = stackList.Length;
        for (int index = 0; index < itemControllers.Length; ++index)
        {
            XUiC_ItemStack itemController = (XUiC_ItemStack)itemControllers[index];
            itemController.InfoWindow = childByType;
            itemController.SlotNumber = index;
            itemController.SlotChangedEvent -= new XUiEvent_SlotChangedEventHandler(HandleInputSlotChangedEvent);
            itemController.StackLocation = XUiC_ItemStack.StackLocationTypes.LootContainer;
            if (index < length)
            {
                itemController.ForceSetItemStack(TileEntity.InputItems[index].Clone());
                itemControllers[index].ViewComponent.IsVisible = true;
                itemController.SlotChangedEvent += new XUiEvent_SlotChangedEventHandler(HandleInputSlotChangedEvent);
            }
            else
            {
                itemController.ItemStack = ItemStack.Empty.Clone();
                itemControllers[index].ViewComponent.IsVisible = false;
            }
        }
        windowGroup.Controller.SetAllChildrenDirty();
    }

    public void SetOutputSlots(ITileEntityProducer lootContainer, ItemStack[] stackList)
    {
        if (stackList == null)
            return;
        TileEntity = lootContainer;
        if (TileEntity == null || TileEntity.OutputItems == null) return;
        items = TileEntity.OutputItems;
        XUiC_ItemInfoWindow childByType = xui.GetChildByType<XUiC_ItemInfoWindow>();
        XUiV_Grid viewComponent = (XUiV_Grid)this.viewComponent;
        viewComponent.Columns = LootContainer.GetLootContainer(lootContainer.OutputList).size.x;
        viewComponent.Rows = LootContainer.GetLootContainer(lootContainer.OutputList).size.y;
        int length = stackList.Length;
        for (int index = 0; index < itemControllers.Length; ++index)
        {
            XUiC_ItemStack itemController = (XUiC_ItemStack)itemControllers[index];
            itemController.InfoWindow = childByType;
            itemController.SlotNumber = index;
            itemController.SlotChangedEvent -= new XUiEvent_SlotChangedEventHandler(HandleOutputSlotChangedEvent);
            itemController.StackLocation = XUiC_ItemStack.StackLocationTypes.LootContainer;
            if (index < length)
            {
                itemController.ForceSetItemStack(TileEntity.OutputItems[index].Clone());
                itemControllers[index].ViewComponent.IsVisible = true;
                itemController.SlotChangedEvent += new XUiEvent_SlotChangedEventHandler(HandleOutputSlotChangedEvent);
            }
            else
            {
                itemController.ItemStack = ItemStack.Empty.Clone();
                itemControllers[index].ViewComponent.IsVisible = false;
            }
        }
        windowGroup.Controller.SetAllChildrenDirty();
    }
    
    public override void Init()
    {
        base.Init();
        XUiV_Grid viewComponent = (XUiV_Grid)this.viewComponent;
        GridCellSize = new Vector2i(viewComponent.CellWidth, viewComponent.CellHeight);
    }

    public void HandleToolSlotChangedEvent(int slotNumber, ItemStack stack)
    {
        TileEntity.UpdateSlot(slotNumber, stack, TileEntity.ToolsItems);
        TileEntity.SetModified();
        TileEntity.RecipesListWindow.FillGridWithRecipes(TileEntity);
    }

    public void HandleInputSlotChangedEvent(int slotNumber, ItemStack stack)
    {
        TileEntity.UpdateSlot(slotNumber, stack, TileEntity.InputItems);
        TileEntity.SetModified();
        TileEntity.RecipesListWindow.FillGridWithRecipes(TileEntity);
    }

    public void HandleOutputSlotChangedEvent(int slotNumber, ItemStack stack)
    {
        TileEntity.UpdateSlot(slotNumber, stack, TileEntity.OutputItems);
        TileEntity.SetModified();
        TileEntity.RecipesListWindow.FillGridWithRecipes(TileEntity);
    }

    void LocalTileEntity_Destroyed(TileEntity te)
    {
        if (GameManager.Instance == null)
            return;
        if (te == TileEntity)
            xui.playerUI.windowManager.Close("producer");
        else
            te.Destroyed -= new XUiEvent_TileEntityDestroyed(LocalTileEntity_Destroyed);
    }

    public override void OnClose()
    {
        base.OnClose();
        xui.lootContainer = null;
        if (TileEntity == null)
            return;
        var te = TileEntity as TileEntity;
        te.Destroyed -= new XUiEvent_TileEntityDestroyed(LocalTileEntity_Destroyed);
    }
}