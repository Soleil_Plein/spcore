﻿using SPCore.Classes;
using SPCore.Classes.SPClasses;

public class XUiC_RecipeInfoWindow : XUiC_InfoWindow
{
    public ITileEntityProducer TileEntity;
    public XUiC_RecipeInfoToolEntry[] ToolsEntries;
    public XUiC_RecipeInfoIngredientEntry[] IngredientsEntries;
    public XUiC_RecipeInfoItemEntry[] ItemsEntries;
    public XUiC_RecipeInfoBlockEntry[] BlocksEntries;

    public void SetTileEntity(ITileEntityProducer tileEntity)
    {
        foreach (var ToolEntry in ToolsEntries)
        {
            ToolEntry.TileEntity = tileEntity;
        }
        foreach (var IngredientEntry in IngredientsEntries)
        {
            IngredientEntry.TileEntity = tileEntity;
        }
        foreach (var ItemEntry in ItemsEntries)
        {
            ItemEntry.TileEntity = tileEntity;
        }
        foreach (var BlockEntry in BlocksEntries)
        {
            BlockEntry.TileEntity = tileEntity;
        }

        TileEntity = tileEntity;
    }

    public override void Init()
    {
        base.Init();
        ToolsEntries = GetChildrenByType<XUiC_RecipeInfoToolEntry>();
        IngredientsEntries = GetChildrenByType<XUiC_RecipeInfoIngredientEntry>();
        ItemsEntries = GetChildrenByType<XUiC_RecipeInfoItemEntry>();
        BlocksEntries = GetChildrenByType<XUiC_RecipeInfoBlockEntry>();
    }

    public override void Cleanup()
    {
        base.Cleanup();

        foreach (var ToolEntry in ToolsEntries)
        {
            ToolEntry.Cleanup();
        }

        foreach (var IngredientEntry in IngredientsEntries)
        {
            IngredientEntry.Cleanup();
        }

        foreach (var ItemEntry in ItemsEntries)
        {
            ItemEntry.Cleanup();
        }

        foreach (var BlockEntry in BlocksEntries)
        {
            BlockEntry.Cleanup();
        }
    }

    public void SetWindow(ProducerRecipe recipe)
    {
        for (int i = 0; i < recipe.Items.Length; i++)
        {
            var ItemEntry = ItemsEntries[i];
            if (recipe.Items[i] == null) break;
            if (recipe.Items[i] != null)
            {
                var itemClass = ItemClass.GetItem(recipe.Items[i]).ItemClass;
                ItemEntry.SetItem(itemClass.GetIconName(), itemClass.GetLocalizedItemName(), recipe.ItemsCount[i], itemClass.CustomIconTint);
            }
        }

        for (int i = 0; i < recipe.BlockNeeded.Length; i++)
        {
            var BlockEntry = BlocksEntries[i];
            if (recipe.BlockNeeded[i] == null) break;
            if (recipe.BlockNeeded[i] != null)
            {
                var itemClass = ItemClass.GetItem(recipe.BlockNeeded[i]).ItemClass;
                BlockEntry.SetBlock(itemClass.GetIconName(), recipe.BlockNeeded[i], itemClass.GetLocalizedItemName(), recipe.BlockNeededCount[i], recipe.BlockSearchRange, itemClass.CustomIconTint);
            }
        }

        for (int i = 0; i < recipe.ToolRequired.Length; i++)
        {
            var ToolEntry = ToolsEntries[i];
            if (recipe.ToolRequired[i] == null) break;
            if (recipe.ToolRequired[i] != null)
            {
                var itemClass = ItemClass.GetItem(recipe.ToolRequired[i]).ItemClass;
                ToolEntry.SetSprite(itemClass.GetIconName(), itemClass.GetLocalizedItemName(), itemClass.CustomIconTint);
                ToolEntry.SetToolState(recipe.ToolRequired[i]);
            }
        }

        for (int i = 0; i < recipe.Ingredients.Length; i++)
        {
            var IngredientEntry = IngredientsEntries[i];
            if (recipe.Ingredients[i] == null) break;
            if (recipe.Ingredients[i] != null)
            {
                var itemClass = ItemClass.GetItem(recipe.Ingredients[i]).ItemClass;
                IngredientEntry.SetSprite(recipe.Ingredients[i], itemClass.GetLocalizedItemName(), itemClass.CustomIconTint);
                IngredientEntry.SetIngredientCount(recipe.Ingredients[i].ToString(), recipe.IngredientsCount[i]);
            }
        }
    }
}