﻿using SPCore.Classes.SPClasses;
using System.Collections.Generic;

public interface ITileEntityProducer : ITileEntity
{
    ProducerRecipe ActualRecipe { get; set; }

    XUiC_ProducerInput InputWindow { get; set; }

    XUiC_ProducerOutput OutputWindow { get; set; }

    XUiC_ProducerTools ToolsWindow { get; set; }

    XUiC_ProducerRecipesList RecipesListWindow { get; set; }

    XUiC_RecipeInfoWindow RecipeInfoWindow { get; set; }

    string WindowGroup { get; set; }

    bool IsRunning { get; set; }

    string[] AllowedToolsItems { get; }

    bool RandomRecipe { get; }

    string InputList { get; set; }

    string OutputList { get; set; }

    string ToolsList { get; set; }

    ItemStack[] InputItems { get; set; }

    ItemStack[] OutputItems { get; set; }

    ItemStack[] ToolsItems { get; set; }

    List<ProducerRecipe> ProducerRecipes { get; }

    bool CheckNearbyBlocks(ProducerRecipe recipe);

    bool CheckNearbyBlock(string blockName, int count, int searchRange, out int result);

    bool ContainerHaveAllTools(string[] tools);

    bool ContainerHaveTool(string itemName);

    bool ContainerHaveIngredients(string[] ingredients, int[] counts);

    bool ContainerHasItem(ItemValue _item, int count);

    bool CanCraftRecipe(ProducerRecipe producerRecipe);

    void UpdateSlot(int _idx, ItemStack _item, ItemStack[] destination, bool isTileEntity = true);

    bool TryStackItem(int startIndex, ItemStack _itemStack, ItemStack[] destination, bool isTileEntity = true);

    bool AddItem(ItemStack _item, ItemStack[] destination, bool isTileEntity = true);

    void SetWindows(XUiC_ProducerInput inputWindow, XUiC_ProducerOutput outputWindow, XUiC_ProducerTools toolsWindow, XUiC_ProducerRecipesList recipesListWindow, XUiC_RecipeInfoWindow recipeInfoWindow);

    void Start();

    void Stop();

    void UpdateVisible();
}