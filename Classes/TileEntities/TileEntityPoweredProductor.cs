﻿using SPCore.Classes;
using SPCore.Classes.SPClasses;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileEntityPoweredProductor : TileEntityPowered, ITileEntityProducer
{
    ItemStack[] inputItems;
    ItemStack[] outputItems;
    ItemStack[] toolsItems;
    string inputList;
    string outputList;
    string toolsList;
    ItemStack[] toolsNet;
    public bool IsCrafting = false;
    IEnumerator coroutine;

    public ProducerRecipe ActualRecipe { get; set; }
    public XUiC_ProducerInput InputWindow { get; set; }
    public XUiC_ProducerOutput OutputWindow { get; set; }
    public XUiC_ProducerTools ToolsWindow { get; set; }
    public XUiC_ProducerRecipesList RecipesListWindow { get; set; }
    public XUiC_RecipeInfoWindow RecipeInfoWindow { get; set; }
    public string WindowGroup { get; set; }
    public bool IsRunning { get; set; }

    public string[] AllowedToolsItems
    {
        get
        {
            DynamicProperties BlockProperties = blockValue.Block.Properties;
            if (BlockProperties.Values.ContainsKey("AllowedToolsItems"))
            {
                return BlockProperties.Values["AllowedToolsItems"].Split(',');
            }
            return null;
        }
    }

    public bool RandomRecipe
    {
        get
        {
            DynamicProperties BlockProperties = blockValue.Block.Properties;
            return BlockProperties.GetBool("RandomRecipe");
        }
    }

    public TileEntityPoweredProductor(Chunk _chunk) : base(_chunk)
    {
    }

    public override void UpdateTick(World world)
    {
        base.UpdateTick(world);
        if (IsPowered && !IsCrafting && (!GameManager.Instance.World.IsRemote() || GameManager.IsDedicatedServer))
        {
            ProduceItems();
        }
        UpdateVisible();
    }

    public void UpdateVisible()
    {
        if (!(GameManager.Instance.World.GetBlock(ToWorldPos()).Block is BlockPoweredProductor block))
            return;
        block.UpdateVisible(this);
    }

    bool IsToolsSame(ItemStack[] _tools)
    {
        if (_tools == null || _tools.Length != ToolsItems.Length)
            return false;
        for (int index = 0; index < _tools.Length; ++index)
        {
            if (!_tools[index].Equals(ToolsItems[index]))
                return false;
        }
        return true;
    }

    public void SetWindows(XUiC_ProducerInput inputWindow, XUiC_ProducerOutput outputWindow, XUiC_ProducerTools toolsWindow, XUiC_ProducerRecipesList recipesListWindow, XUiC_RecipeInfoWindow recipeInfoWindow)
    {
        InputWindow = inputWindow;
        OutputWindow = outputWindow;
        ToolsWindow = toolsWindow;
        RecipesListWindow = recipesListWindow;
    }

    public string InputList
    {
        get => inputList;
        set
        {
            inputList = value;
            inputItems = ItemStack.CreateArray(LootContainer.GetLootContainer(inputList).size.x * LootContainer.GetLootContainer(inputList).size.y);
        }
    }

    public string OutputList
    {
        get => outputList;
        set
        {
            outputList = value;
            outputItems = ItemStack.CreateArray(LootContainer.GetLootContainer(outputList).size.x * LootContainer.GetLootContainer(outputList).size.y);
        }
    }

    public string ToolsList
    {
        get => toolsList;
        set
        {
            toolsList = value;
            toolsItems = ItemStack.CreateArray(LootContainer.GetLootContainer(toolsList).size.x * LootContainer.GetLootContainer(toolsList).size.y);
        }
    }

    public List<ProducerRecipe> ProducerRecipes
    {
        get
        {
            BlockPoweredProductor blockProductor = (BlockPoweredProductor)blockValue.Block;
            return blockProductor.ProducerRecipes;
        }
    }

    TileEntityPoweredProductor(TileEntityPoweredProductor _other) : base(null)
    {
        inputItems = ItemStack.Clone(_other.inputItems);
        outputItems = ItemStack.Clone(_other.outputItems);
        toolsItems = ItemStack.Clone(_other.toolsItems);
    }

    public override TileEntity Clone() => new TileEntityPoweredProductor(this);

    public ItemStack[] InputItems
    {
        get
        {
            if (inputItems == null)
                inputItems = ItemStack.CreateArray(LootContainer.GetLootContainer(inputList).size.x * LootContainer.GetLootContainer(inputList).size.y);
            return inputItems;
        }
        set => inputItems = value;
    }

    public ItemStack[] OutputItems
    {
        get
        {
            if (outputItems == null)
                outputItems = ItemStack.CreateArray(LootContainer.GetLootContainer(outputList).size.x * LootContainer.GetLootContainer(outputList).size.y);
            return outputItems;
        }
        set => outputItems = value;
    }

    public ItemStack[] ToolsItems
    {
        get
        {
            if (toolsItems == null)
                toolsItems = ItemStack.CreateArray(LootContainer.GetLootContainer(toolsList).size.x * LootContainer.GetLootContainer(toolsList).size.y);
            return toolsItems;
        }
        set
        {
            toolsItems = value;
            UpdateVisible();
        }
    }

    public bool ContainerHaveAllTools(string[] tools)
    {
        foreach (var tool in tools)
        {
            if (!ContainerHaveTool(tool)) return false;
        }
        return true;
    }

    public bool ContainerHaveTool(string itemName)
    {
        if (itemName == null) return true;
        foreach (ItemStack item in ToolsItems)
        {
            if (!item.IsEmpty() && item.itemValue.ItemClass.Name.EqualsCaseInsensitive(itemName))
            {
                if (item.itemValue.HasQuality && item.itemValue.UseTimes >= item.itemValue.MaxUseTimes) return false;
                return true;
            }
        }
        return false;
    }

    public bool ContainerHaveIngredients(string[] ingredients, int[] counts)
    {
        for (int index = 0; index < ingredients.Length; index++)
        {
            var itemName = ingredients[index];
            var count = counts[index];
            var itemValue = ItemClass.GetItem(itemName);
            if (!ContainerHasItem(itemValue, count)) return false;
        }
        return true;
    }

    public bool ContainerHasItem(ItemValue _item, int count)
    {
        int count1 = 0;
        for (int index = 0; index < InputItems.Length; ++index)
        {
            if (InputItems[index].itemValue.type == _item.type)
            {
                count1 += InputItems[index].count;
            }
        }
        if (count1 >= count) return true;
        return false;
    }

    void tileEntityChanged()
    {
        for (int i = 0; i < listeners.Count; i++)
        {
            listeners[i].OnTileEntityChanged(this, 0);
        }
        SetModified();
    }

    public void UpdateSlot(int _idx, ItemStack _item, ItemStack[] destination, bool isTileEntity = true)
    {
        destination[_idx] = _item.Clone();
        if (isTileEntity) tileEntityChanged();
    }

    public bool TryStackItem(int startIndex, ItemStack _itemStack, ItemStack[] destination, bool isTileEntity = true)
    {
        int count1 = _itemStack.count;
        for (int index = startIndex; index < destination.Length; ++index)
        {
            int count2 = _itemStack.count;
            if (_itemStack.itemValue.type == destination[index].itemValue.type && destination[index].CanStackPartly(ref count2))
            {
                destination[index].count += count2;
                _itemStack.count -= count2;
                if (isTileEntity) SetModified();
                if (_itemStack.count == 0)
                {
                    tileEntityChanged();
                    SetModified();
                    return true;
                }
            }
        }
        if (_itemStack.count != count1)
        {
            if (isTileEntity)
            {
                SetModified();
                tileEntityChanged();
            }
        }
        return false;
    }

    public bool AddItem(ItemStack _item, ItemStack[] destination, bool isTileEntity = true)
    {
        for (int _idx = 0; _idx < destination.Length; ++_idx)
        {
            if (destination[_idx].IsEmpty())
            {
                UpdateSlot(_idx, _item, destination, isTileEntity);
                if (isTileEntity) SetModified();
                return true;
            }
        }
        return false;
    }

    public bool DeleteItems(string[] ingredients, int[] counts)
    {
        for (int _idx = 0; _idx < ingredients.Length; ++_idx)
        {
            string ingredient = ingredients[_idx];
            int count = counts[_idx];
            var itemStack = new ItemStack(ItemClass.GetItem(ingredient, true), count);
            DeleteItem(itemStack);
        }
        return false;
    }

    public void DegradeTools(ProducerRecipe recipe)
    {
        for (int i = 0; i < ToolsItems.Length; i++)
        {
            var toolItemStack = ToolsItems[i];
            foreach (var tool in recipe.ToolRequired)
            {
                if (toolItemStack.itemValue.HasQuality && toolItemStack.itemValue.ItemClass.Name == tool && toolItemStack.itemValue.UseTimes < toolItemStack.itemValue.MaxUseTimes)
                {
                    //Log.Warning("Degradation de l'outil");
                    ToolsItems[i].itemValue.UseTimes += recipe.ToolDegradation;
                    SetModified();
                    return;
                }
            }
        }
    }

    public void DeleteItem(ItemStack _item)
    {
        int count = _item.count;
        for (int _idx = 0; _idx < InputItems.Length; ++_idx)
        {
            var itemStack = InputItems[_idx];
            if (!itemStack.IsEmpty() && _item.itemValue.type == itemStack.itemValue.type)
            {
                if (itemStack.count > count)
                {
                    UpdateSlot(_idx, new ItemStack(itemStack.itemValue, itemStack.count - count), InputItems);
                    return;
                }
                else if (itemStack.count <= count)
                {
                    count -= itemStack.count;
                    UpdateSlot(_idx, new ItemStack().Clone(), InputItems);
                }
                SetModified();
            }
        }
    }

    public bool CheckNearbyBlock(string blockName, int count, int searchRange, out int result)
    {
        var world = GameManager.Instance.World;
        Vector3i pos = new Vector3i(ToWorldPos());
        int index = 0;
        for (int _y = pos.y - searchRange; _y <= pos.y; ++_y)
        {
            for (int _x = pos.x - searchRange / 2; _x <= pos.x + searchRange / 2; ++_x)
            {
                for (int _z = pos.z - searchRange / 2; _z <= pos.z + searchRange / 2; ++_z)
                {
                    if (world.GetBlock(_x, _y, _z).Block.GetBlockName() == blockName)
                    {
                        index++;
                    }
                }
            }
        }
        result = index;
        return count <= index;
    }

    public bool CheckNearbyBlocks(ProducerRecipe recipe)
    {
        int[] counts = (int[])recipe.BlockNeededCount.Clone();
        string[] blocks = (string[])recipe.BlockNeeded.Clone();
        if (recipe.BlockNeeded[0] == null && recipe.BlockNeededCount[0] == 0) return true;
        if (recipe.BlockNeeded[0] != null && recipe.BlockNeededCount[0] != 0 && recipe.BlockSearchRange > 0)
        {
            if (recipe.BlockNeeded[0] == null) return true;
            var world = GameManager.Instance.World;
            Vector3i pos = new Vector3i(ToWorldPos());
            for (int _y = pos.y - recipe.BlockSearchRange; _y <= pos.y; ++_y)
            {
                for (int _x = pos.x - recipe.BlockSearchRange / 2; _x <= pos.x + recipe.BlockSearchRange / 2; ++_x)
                {
                    for (int _z = pos.z - recipe.BlockSearchRange / 2; _z <= pos.z + recipe.BlockSearchRange / 2; ++_z)
                    {
                        for (int i = 0; i < blocks.Length; i++)
                        {
                            if (world.GetBlock(_x, _y, _z).Block.GetBlockName() == blocks[i])
                            {
                                if (blocks[i] == null) counts[i] = 0;
                                if (counts[i] > 0) counts[i]--;
                            }
                        }
                    }
                }
            }
            foreach (var count in counts)
            {
                if (count != 0) return false;
            }
            return true;
        }
        return false;
    }

    public bool CanAddItems(ProducerRecipe recipe)
    {
        ItemStack[] itemStacks = ItemStack.CreateArray(OutputItems.Length);
        for (int i = 0; i < itemStacks.Length; i++)
        {
            itemStacks[i] = OutputItems[i].Clone();
        }

        for (int i = 0; i < recipe.Items.Length; i++)
        {
            var item = new ItemStack(ItemClass.GetItem(recipe.Items[i]), recipe.ItemsCount[i]);
            if (!TryStackItem(0, item, itemStacks, false))
            {
                if (!AddItem(item, itemStacks, false))
                    return false;
            }
        }
        return true;
    }

    public bool CanCraftRecipe(ProducerRecipe producerRecipe)
    {
        return (ContainerHaveAllTools(producerRecipe.ToolRequired) || producerRecipe.ToolRequired == null) && ContainerHaveIngredients(producerRecipe.Ingredients, producerRecipe.IngredientsCount) && CheckNearbyBlocks(producerRecipe);
    }

    public ProducerRecipe GetRandomRecipe()
    {
        List<ProducerRecipe> recipes = new List<ProducerRecipe>();
        foreach (var recipe in ProducerRecipes)
        {
            if (CanCraftRecipe(recipe)) recipes.Add(recipe);
        }
        if (recipes.Count > 0) return recipes.RandomObject();
        return null;
    }

    public ProducerRecipe PickRecipe()
    {
        if (RandomRecipe) return GetRandomRecipe();
        else
        {
            for (int i = ProducerRecipes.Count - 1; i >= 0; i--)
            {
                var recipe = ProducerRecipes[i];
                if (CanCraftRecipe(recipe)) return recipe;
            }
            return null;
        }
    }

    public void ProduceItems()
    {
        var recipe = PickRecipe();
        if (recipe == null) return;
        if ((ContainerHaveAllTools(recipe.ToolRequired) || recipe.ToolRequired == null) && ContainerHaveIngredients(recipe.Ingredients, recipe.IngredientsCount) && CanAddItems(recipe) && CheckNearbyBlocks(recipe))
        {
            IsCrafting = true;
            coroutine = CraftItemWithDelay(recipe);
            GameManager.Instance.StartCoroutine(coroutine);
            //Log.Warning($"Lancement d'un craft, temps estimé : {recipe.CraftTime} secondes");
        }
    }

    IEnumerator CraftItemWithDelay(ProducerRecipe recipe)
    {
        yield return new WaitForSeconds(recipe.CraftTime);
        if (CanCraftRecipe(recipe) && this != null)
        {
            int random;
            for (int i = 0; i < recipe.Items.Length; i++)
            {
                random = Random.Range(1, 101);
                if (random < recipe.Probability[i] * 100)
                {
                    int count = recipe.ItemsCount[i];
                    string itemName = recipe.Items[i];
                    var item = new ItemStack(ItemClass.GetItem(itemName), count);
                    //Log.Warning($"Traitement de {itemName}/{count}");
                    if (!TryStackItem(0, item, OutputItems))
                    {
                        //Log.Warning($"Echec du TryStack de l'item {itemName}/{count}");
                        if (AddItem(item, OutputItems))
                        {
                            //Log.Warning($"Ajout de l'item {itemName}/{count}");
                        }
                        //Log.Warning($"Echec du AddItem de l'item {itemName}/{count}");
                    }
                }
            }
            if (recipe.Buffs[0] != null) DiffuseBuffs(recipe);
            DeleteItems(recipe.Ingredients, recipe.IngredientsCount);
            if (recipe.ToolRequired != null && recipe.ToolDegradation > 0) DegradeTools(recipe);
            if (ToolsWindow != null && InputWindow != null && OutputWindow != null)
            {
                if (recipe.ToolRequired[0] != null) ToolsWindow.SetToolsSlots(this, ToolsItems);
                InputWindow.SetInputSlots(this, InputItems);
                OutputWindow.SetOutputSlots(this, OutputItems);
            }
            if (blockValue.Block.Properties.GetFloat("HeatMapStrength") > 0 && blockValue.Block.Properties.GetFloat("HeatMapTime") > 0 && blockValue.Block.Properties.GetFloat("HeatMapFrequency") > 0) emitHeatMapEvent(GameManager.Instance.World, EnumAIDirectorChunkEvent.Forge);
        }
        IsCrafting = false;
        //Log.Warning($"Craft terminé");
    }

    public void DiffuseBuffs(ProducerRecipe recipe)
    {
        var players = GameManager.Instance.World.Players.list;
        foreach (var player in players)
        {
            if (Vector3.Distance(ToWorldPos().ToVector3(), player.position) <= recipe.BuffsRange)
            {
                foreach (var buff in recipe.Buffs)
                {
                    player.Buffs.AddBuff(buff);
                }
            }
        }
    }

    public override TileEntityType GetTileEntityType() => TileEntityMapping.Types[TileEntityMapping.PoweredProductor];

    public override void OnReadComplete()
    {
        base.OnReadComplete();
        IsCrafting = false;
    }

    public override void OnUnload(World world)
    {
        base.OnUnload(world);
        if (coroutine != null) GameManager.Instance?.StopCoroutine(coroutine);
    }

    public override void read(PooledBinaryReader _br, StreamModeRead _eStreamMode)
    {
        base.read(_br, _eStreamMode);
        int inputSize = _br.ReadInt32();
        inputItems = ItemStack.CreateArray(inputSize);
        for (int i = 0; i < inputSize; i++)
        {
            inputItems[i].Read(_br);
        }
        int outputSize = _br.ReadInt32();
        outputItems = ItemStack.CreateArray(outputSize);
        for (int i = 0; i < outputSize; i++)
        {
            outputItems[i].Read(_br);
        }
        int toolsSize = _br.ReadInt32();
        toolsItems = ItemStack.CreateArray(toolsSize);
        for (int i = 0; i < toolsSize; i++)
        {
            toolsItems[i].Read(_br);
        }
        inputList = _br.ReadString();
        outputList = _br.ReadString();
        toolsList = _br.ReadString();
        IsCrafting = _br.ReadBoolean();
        WindowGroup = _br.ReadString();
        IsRunning = _br.ReadBoolean();
    }

    public override void write(PooledBinaryWriter _bw, StreamModeWrite _eStreamMode)
    {
        base.write(_bw, _eStreamMode);
        _bw.Write(InputItems.Length);
        foreach (ItemStack item in InputItems)
            item.Clone().Write(_bw);
        _bw.Write(OutputItems.Length);
        foreach (ItemStack item in OutputItems)
            item.Clone().Write(_bw);
        _bw.Write(ToolsItems.Length);
        foreach (ItemStack item in ToolsItems)
            item.Clone().Write(_bw);
        _bw.Write(inputList);
        _bw.Write(outputList);
        _bw.Write(toolsList);
        _bw.Write(IsCrafting);
        _bw.Write(WindowGroup);
        _bw.Write(IsRunning);
    }

    public void Start()
    {
        IsRunning = true;
        SetModified();
    }

    public void Stop()
    {
        if (coroutine != null)
        {
            ThreadManager.StopCoroutine(coroutine);
            coroutine = null;
            IsCrafting = false;
        }
        IsRunning = false;
        SetModified();
    }
}