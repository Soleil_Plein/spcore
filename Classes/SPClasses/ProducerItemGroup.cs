﻿using HarmonyLib;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

namespace SPCore.Classes.SPClasses
{
    public class ProducerItemGroup
    {
        public ProducerItemGroup(string _groupName, bool _selectOnlyOne, string[] _items)
        {
            GroupName = _groupName;
            SelectOnlyOne = _selectOnlyOne;
            Items = _items;
        }

        static Dictionary<string, ProducerItemGroup> List = new Dictionary<string, ProducerItemGroup>();

        public static void Init()
        {
            List.Clear();

            var tempList = new List<ProducerItemGroup>();

            foreach (var mod in ModManager.GetLoadedMods())
            {
                string path = mod.Path + "/Config/producer_groups.xml";

                if (File.Exists(path))
                {
                    var xmlDocument = new XmlDocument();
                    xmlDocument.Load(path);

                    foreach (XmlNode node in xmlDocument.DocumentElement.ChildNodes)
                    {
                        var producerItemGroup = XmlNodeToProducerItemGroup(node);
                        tempList.Add(producerItemGroup);
                    }
                }
            }
            AddGroupsToList(tempList);
            //LogList();
        }

        static ProducerItemGroup XmlNodeToProducerItemGroup(XmlNode _xmlNode)
        {
            string groupName = _xmlNode.Attributes["name"].Value;
            bool selectOnlyOne = StringParsers.ParseBool(_xmlNode.Attributes["selectOnlyOne"].Value);
            string[] items = new string[_xmlNode.ChildNodes.Count];
            int index = 0;
            foreach (XmlNode node in _xmlNode.ChildNodes)
            {
                if (node.Name == "item")
                {
                    items[index] = node.Attributes["name"].Value;
                    index++;
                }
            }
            return new ProducerItemGroup(groupName, selectOnlyOne, items);
        }

        public static bool StringIsProducerItemGroup(string _itemGroupName) => List.ContainsKey(_itemGroupName);

        static void AddGroupsToList(List<ProducerItemGroup> _producerItemGroups)
        {
            foreach (var item in _producerItemGroups)
            {
                List.Add(item.GroupName, item);
            }
        }

        static void LogList()
        {
            foreach (var item in List) Log.Warning(item.ToString());
        }

        public override string ToString() => $"GroupName : {GroupName} / Items : {Items.Join(delimiter: ",")} / SelectOnlyOne : {SelectOnlyOne}";

        public string GroupName { get; private set; }
        public bool SelectOnlyOne { get; private set; }
        public string[] Items { get; private set; }
    }
}
