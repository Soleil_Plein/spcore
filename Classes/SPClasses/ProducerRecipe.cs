﻿using HarmonyLib;
using System.Collections.Generic;
using System.Linq;

namespace SPCore.Classes.SPClasses
{
    public class ProducerRecipe
    {
        public ProducerRecipe(int _buffsRange, string[] _buffs, float[] _prob, int[] _blockNeededCount, string[] _blockNeeded, int _blockSearchRange, bool _hidden, int _craftTime, string[] _toolRequired, int _toolDegradation, string[] _ingredients, string[] _items, int[] _ingredientsCount, int[] _itemsCount, string _randomText, ProducerItemGroup _producerItemGroup)
        {
            BuffsRange = _buffsRange;
            Buffs = _buffs;
            Probability = _prob;
            BlockNeededCount = _blockNeededCount;
            BlockNeeded = _blockNeeded;
            BlockSearchRange = _blockSearchRange;
            Hidden = _hidden;
            CraftTime = _craftTime;
            ToolRequired = _toolRequired;
            ToolDegradation = _toolDegradation;
            Ingredients = _ingredients;
            Items = _items;
            ItemsCount = _itemsCount;
            IngredientsCount = _ingredientsCount;
            RandomText = _randomText;
            ProducerItemGroup = _producerItemGroup;
        }

        public static List<ProducerRecipe> GetRecipes(DynamicProperties dynamicProperties)
        {
            List<ProducerRecipe> recipes = new List<ProducerRecipe>();
            foreach (var item in dynamicProperties.Classes.Dict)
            {
                int _buffsRange;
                string[] _buffs;
                float[] _prob;
                int[] _blockNeededCount;
                string[] _blockNeeded;
                int _blockSearchRange;
                bool _hidden;
                int _craftTime;
                string[] _toolRequired;
                int _toolDegradation;
                string[] _ingredients;
                string[] _items;
                int[] _ingredientsCount;
                int[] _itemsCount;
                string _randomText = null;
                ProducerItemGroup _producerItemGroup = null;

                _buffsRange = 0;
                _buffs = new string[] { null };
                _blockNeededCount = new int[] { 0 };
                _toolRequired = new string[1] { null };
                _blockNeeded = new string[1] { null };
                _blockSearchRange = 0;
                _hidden = false;
                _craftTime = 30;
                _toolDegradation = 0;
                if (item.Value.Values["ItemsProbabilities"].Contains(","))
                {
                    string[] ints1 = item.Value.Values["ItemsProbabilities"].Split(',');
                    int length1 = ints1.Length;
                    _prob = new float[length1];
                    for (int index = 0; index < ints1.Length; index++)
                    {
                        _prob[index] = StringParsers.ParseFloat(ints1[index]);
                    }
                }
                else _prob = new float[1] { StringParsers.ParseFloat(item.Value.Values["ItemsProbabilities"]) };

                if (item.Value.Values.TryGetString("BlockCountNeeded", out string result))
                {
                    string[] ints2 = result.Split(',');
                    int length2 = ints2.Length;
                    _blockNeededCount = new int[length2];
                    for (int index = 0; index < ints2.Length; index++)
                    {
                        _blockNeededCount[index] = StringParsers.ParseSInt32(ints2[index]);
                    }
                }
                
                if (item.Value.Values.TryGetString("BlockNameNeeded", out string result1))
                {
                    _blockNeeded = result1.Split(',');
                }

                if (item.Value.Values.TryGetString("Hidden", out string result2))
                {
                    _hidden = StringParsers.ParseBool(result2);
                }

                if (item.Value.Values.TryGetString("BlocksSearchRange", out string result3))
                {
                    _blockSearchRange = StringParsers.ParseSInt32(result3);
                }

                if (item.Value.Values.TryGetString("CraftTime", out string result4))
                {
                    _craftTime = StringParsers.ParseSInt32(result4);
                }

                if (item.Value.Values.TryGetString("Tool", out string result5))
                {
                    _toolRequired = result5.Split(',');
                }

                if (item.Value.Values.TryGetString("ToolDegradation", out string result6))
                {
                    _toolDegradation = StringParsers.ParseSInt32(result6);
                }

                if (item.Value.Values.TryGetString("Buffs", out string result7))
                {
                    _buffs = result7.Split(',');
                }

                if (item.Value.Values.TryGetString("BuffsRange", out string result8))
                {
                    _buffsRange = StringParsers.ParseSInt32(result8);
                }

                if (item.Value.Values.TryGetString("RandomTextKey", out string result9))
                {
                    _randomText = result9;
                }

                if (item.Value.Values["Ingredients"].Contains(","))
                {
                    _ingredients = item.Value.Values["Ingredients"].Split(',');
                }
                else _ingredients = new string[1] { item.Value.Values["Ingredients"] };

                _items = item.Value.Values["Items"].Split(',');

                string[] ints3 = item.Value.Values["IngredientsCount"].Split(',');
                int length3 = ints3.Length;
                _ingredientsCount = new int[length3];
                for (int index = 0; index < ints3.Length; index++)
                {
                    _ingredientsCount[index] = StringParsers.ParseSInt32(ints3[index]);
                }

                string[] ints4 = item.Value.Values["ItemsCount"].Split(',');
                int length4 = ints4.Length;
                _itemsCount = new int[length4];
                for (int index = 0; index < ints4.Length; index++)
                {
                    _itemsCount[index] = StringParsers.ParseSInt32(ints4[index]);
                }

                ProducerRecipe producerRecipe = new ProducerRecipe(_buffsRange, _buffs, _prob, _blockNeededCount, _blockNeeded, _blockSearchRange, _hidden, _craftTime, _toolRequired, _toolDegradation, _ingredients, _items, _ingredientsCount, _itemsCount, _randomText, _producerItemGroup);
                recipes.Add(producerRecipe);
            }
            //LogRecipes(recipes);
            return recipes;
        }

        public static void LogRecipes(List<ProducerRecipe> producerRecipes)
        {
            int index = 1;
            foreach (var producerRecipe in producerRecipes)
            {
                Log.Warning($"Recipe #{index} :\n\t{producerRecipe}");
                index++;
            }
        }

        public override string ToString()
        {
            return $"items : {Items.Join(delimiter: "/")}\n\t" +
                    $"ingredientsCount : {IngredientsCount.Join(delimiter: "/")}\n\t" +
                    $"itemsCount : {ItemsCount.Join(delimiter: "/")}\n\t" +
                    $"itemsProbabilities : {Probability.Join(delimiter: "/")}\n\t" +
                    $"ingredients : {Ingredients.Join(delimiter: "/")}\n\t" +
                    $"blockNeeded : {BlockNeeded} / {BlockNeededCount} / {BlockSearchRange}\n\t" +
                    $"hidden : {Hidden}\n\t" +
                    $"craftTime : {CraftTime}";
        }

        public bool IsRandom
        {
            get
            {
                bool flag = false;
                foreach (var prob in Probability)
                {
                    Log.Warning(prob.ToString());
                    if (prob < 1f)
                    {
                        flag = true;
                        return flag;
                    }
                }
                return flag;
            }
        }

        public int BuffsRange { get; private set; }

        public string[] Buffs { get; private set; }

        public float[] Probability { get; private set; }

        public int[] BlockNeededCount { get; private set; }

        public string[] BlockNeeded { get; private set; }

        public int BlockSearchRange { get; private set; }

        public bool Hidden { get; private set; }

        public int CraftTime { get; private set; }

        public string[] ToolRequired { get; private set; }

        public int ToolDegradation { get; private set; }

        public string[] Items { get; private set; }

        public string[] Ingredients { get; private set; }

        public int[] IngredientsCount { get; private set; }

        public int[] ItemsCount { get; private set; }

        public string RandomText { get; private set; }

        public ProducerItemGroup ProducerItemGroup { get; private set; }
    }
}