﻿using HarmonyLib;
using SPCore.Classes;
using System;

namespace SPCore.Harmony
{
    public class H_GameManager
    {
        [HarmonyPatch(typeof(GameManager), "OpenTileEntityUi", new Type[] { typeof(int), typeof(TileEntity), typeof(string) })]
        public class H_GameManager_OpenTileEntityUi
        {
            static void Postfix(GameManager __instance, int _entityIdThatOpenedIt, TileEntity _te, string _customUi, World ___m_World)
            {
                LocalPlayerUI uiForPlayer = LocalPlayerUI.GetUIForPlayer(___m_World.GetEntity(_entityIdThatOpenedIt) as EntityPlayerLocal);
                if (string.IsNullOrEmpty(_customUi))
                {
                    if (_te is ITileEntityProducer)
                        productorOpened((ITileEntityProducer)_te, uiForPlayer);
                }
            }

            static void productorOpened(ITileEntityProducer _te, LocalPlayerUI _playerUI)
            {
                if (_playerUI == null)
                    return;
                var window = _playerUI.windowManager.GetWindow(_te.WindowGroup);
                ((XUiC_ProducerWindowGroup)((XUiWindowGroup)window).Controller).SetTileEntity(_te);
                _playerUI.windowManager.Open(window, true);
            }
        }
    }
}