﻿using HarmonyLib;
using SPCore.Classes.SPClasses;
using System;

namespace SPCore.Harmony
{
    public class H_WorldStaticData
    {
        [HarmonyPatch(typeof(WorldStaticData), "LoadAllXmlsCo", new Type[] { typeof(bool), typeof(WorldStaticData.ProgressDelegate) })]
        public class H_WorldStaticData_LoadAllXmlsCo
        {
            static void Postfix()
            {
                ProducerItemGroup.Init();
            }
        }
    }
}