﻿using Audio;
using HarmonyLib;
using System.Reflection;
using UnityEngine;

namespace SPCore.Harmony
{
    public class H_XUiC_ItemStack
    {
        [HarmonyPatch(typeof(XUiC_ItemStack), "SelectedChanged")]
        public class H_XUiC_ItemStack_SelectedChanged
        {
            static void Postfix(XUiC_ItemStack __instance)
            {
                var recipeInfoWindow = __instance.xui.GetChildByType<XUiC_RecipeInfoWindow>();
                if (recipeInfoWindow != null)
                {
                    recipeInfoWindow.ViewComponent.IsVisible = false;
                }
            }
        }

        [HarmonyPatch(typeof(XUiC_ItemStack), "HandleMoveToPreferredLocation")]
        public class H_XUiC_ItemStack_HandleMoveToPreferredLocation
        {
            static MethodInfo HandleSlotChangeEventMethod = AccessTools.Method(typeof(XUiC_ItemStack), "HandleSlotChangeEvent");

            static bool Prefix(XUiC_ItemStack __instance, ref AudioClip ___placeSound)
            {
                if (__instance.StackLocation == XUiC_ItemStack.StackLocationTypes.Backpack || __instance.StackLocation == XUiC_ItemStack.StackLocationTypes.ToolBelt)
                {
                    if (API.CurrentProducerTools != null && API.CurrentProducerTools.TryAddTool(__instance.ItemStack.itemValue.ItemClass, __instance.ItemStack))
                    {
                        __instance.ItemStack = ItemStack.Empty.Clone();
                        if (___placeSound != null)
                            Manager.PlayXUiSound(___placeSound, 0.75f);
                        HandleSlotChangeEventMethod.Invoke(__instance, new object[] { });
                        return false;
                    }
                    if (API.CurrentProducerInput != null)
                    {
                        if (!API.CurrentProducerInput.TileEntity.TryStackItem(0, __instance.ItemStack, API.CurrentProducerInput.TileEntity.InputItems))
                        {
                            if (API.CurrentProducerInput.TileEntity.AddItem(__instance.ItemStack, API.CurrentProducerInput.TileEntity.InputItems))
                            {
                                __instance.ItemStack = ItemStack.Empty.Clone();
                                if (___placeSound != null)
                                    Manager.PlayXUiSound(___placeSound, 0.75f);
                                HandleSlotChangeEventMethod.Invoke(__instance, new object[] { });
                                API.CurrentProducerInput.SetInputSlots(API.CurrentProducerInput.TileEntity, API.CurrentProducerInput.TileEntity.InputItems);
                                return false;
                            }
                            return true;
                        }
                        __instance.ItemStack = ItemStack.Empty.Clone();
                        if (___placeSound != null)
                            Manager.PlayXUiSound(___placeSound, 0.75f);
                        HandleSlotChangeEventMethod.Invoke(__instance, new object[] { });
                        API.CurrentProducerInput.SetInputSlots(API.CurrentProducerInput.TileEntity, API.CurrentProducerInput.TileEntity.InputItems);
                        return false;
                    }
                }
                return true;
            }
        }
    }
}