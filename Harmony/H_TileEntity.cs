﻿using HarmonyLib;

namespace SPCore.Harmony
{
    public class H_TileEntity
    {
        [HarmonyPatch(typeof(TileEntity), "Instantiate")]
        public class H_TileEntity_Instantiate
        {
            static bool Prefix(TileEntityType type, Chunk _chunk, ref TileEntity __result)
            {
                bool flag = true;
                if (type == TileEntityMapping.Types[TileEntityMapping.Productor])
                {
                    flag = false;
                    Log.Warning("Instantiate TileEntityProductor");
                    __result = new TileEntityProductor(_chunk);
                    return flag;
                }
                else if (type == TileEntityMapping.Types[TileEntityMapping.PoweredProductor])
                {
                    flag = false;
                    Log.Warning("Instantiate TileEntityPoweredProductor");
                    __result = new TileEntityPoweredProductor(_chunk);
                    return flag;
                }
                return flag;
            }
        }
    }
}